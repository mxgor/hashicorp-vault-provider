package main

import (
	"testing"

	"github.com/spf13/viper"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/engine/test"
)

func init() {
	viper.SetConfigFile(".env")
	viper.ReadInConfig()
	viper.AutomaticEnv()
}

func Test_signing_rsa4096(t *testing.T) {
	vaultProvider := new(VaultCryptoProvider)
	if !test.Sign_Testing_Rsa4096(vaultProvider) {
		t.Error()
	}
}

func Test_encryption_aes256(t *testing.T) {
	vaultProvider := new(VaultCryptoProvider)
	if !test.Encryption_Testing_Aes256(vaultProvider) {
		t.Error()
	}
}

func Test_encryption_ed(t *testing.T) {
	localProvider := new(VaultCryptoProvider)
	if !test.Sign_Testing_Ed(localProvider) {
		t.Error()
	}
}

func Test_GetKeys(t *testing.T) {
	localProvider := new(VaultCryptoProvider)
	b, err := test.GetKeys_Test(localProvider)

	if !b {
		t.Error(err)
	}
}
