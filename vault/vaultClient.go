package vault

import (
	"context"
	"crypto/ed25519"
	"crypto/x509"
	"encoding/base64"
	b64 "encoding/base64"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"net/http"
	"reflect"
	"regexp"
	"strconv"
	"strings"

	vaultpkg "github.com/hashicorp/vault/api"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/engine/core/types"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/engine/plugins/hashicorp-vault-provider/utils"
)

const (
	pathSysMounts = "/v1/sys/mounts"
)

type VaultClientConfig struct {
	Address string
	Token   string
}

type VaultKeyDescription struct {
	KeyName string
	Key     []byte
	Type    types.KeyType
	Version string
}

type VaultParameter struct {
	Client      *vaultpkg.Client
	Context     context.Context
	EnginePath  string
	Description string
}
type VaultKeyParameter struct {
	KeyName string
	Vault   VaultParameter
}

type VaultKeyTypeParameter struct {
	KeyParameter VaultKeyParameter
	KeyType      types.KeyType
}

type VaultHashParameter struct {
	KeyParameter  VaultKeyParameter
	HashAlgorithm types.HashAlgorithm
}

func engineExists(vaultParameter VaultParameter) error {
	b, err := VaultEngineExists(vaultParameter)

	if err != nil || !b {

		if !b {
			err = errors.New("crypto engine not found")
		}
		return &types.CryptoContextError{Err: err}
	}
	return nil
}

func VaultGetNamespaces(parameter VaultParameter) ([]string, error) {
	engines, err := parameter.Client.Sys().ListMounts()
	if err != nil {
		if e, ok := err.(*vaultpkg.ResponseError); ok {
			return nil, e
		}
		return nil, err
	}

	var namespaces []string
	namespaces = make([]string, 0)
	for name, engine := range engines {
		if engine.Type == "transit" {

			if parameter.EnginePath != "" {
				if strings.Contains(name, parameter.EnginePath) {
					n := strings.Trim(name, "/")
					namespaces = append(namespaces, n)
				}
			} else {
				n := strings.Trim(name, "/")
				namespaces = append(namespaces, n)
			}
		}
	}

	return namespaces, nil
}

func VaultGetKey(vaultParameter VaultKeyParameter) ([]VaultKeyDescription, error) {

	err := engineExists(vaultParameter.Vault)

	if err != nil {
		return nil, err
	}

	URL := "/v1/" + vaultParameter.Vault.EnginePath + "/keys/" + vaultParameter.KeyName
	body := make(map[string]interface{})

	return basicVaultCall[map[string]interface{}, []VaultKeyDescription]("GET", URL, body, "keys", func(keys map[string]interface{}, data map[string]interface{}) ([]VaultKeyDescription, error) {
		var m = make([]VaultKeyDescription, 0)
		t := data["type"].(string)
		var inter interface{}
		for i, v := range keys {
			if reflect.TypeOf(v) == reflect.MapOf(reflect.TypeOf("string"), reflect.TypeOf(&inter).Elem()) {
				key := v.(map[string]interface{})
				pubkey := []byte(key["public_key"].(string))

				if t == string(types.Ed25519) {
					dec, err := base64.StdEncoding.DecodeString(string(pubkey))
					if err != nil {
						return nil, err
					}

					bytes, err := x509.MarshalPKIXPublicKey(ed25519.PublicKey(dec))

					if err != nil {
						return nil, err
					}

					pemBlock := &pem.Block{
						Type:  "PUBLIC KEY",
						Bytes: bytes,
					}

					pubkey_bytes := pem.EncodeToMemory(pemBlock)

					pubkey = []byte(pubkey_bytes)
				}

				desc := VaultKeyDescription{
					KeyName: vaultParameter.KeyName,
					Type:    types.KeyType(t),
					Version: i,
					Key:     pubkey,
				}
				m = append(m, desc)
			} else {
				desc := VaultKeyDescription{
					KeyName: vaultParameter.KeyName,
					Type:    types.KeyType(t),
					Version: i,
					Key:     nil,
				}
				m = append(m, desc)
			}
		}
		return m, nil
	}, vaultParameter.Vault)
}

func VaultListKeys(vaultParameter VaultParameter, regex regexp.Regexp) ([]string, error) {

	err := engineExists(vaultParameter)

	if err != nil {
		return nil, err
	}

	URL := "/v1/" + vaultParameter.EnginePath + "/keys"
	body := make(map[string]interface{})

	if regex.String() == "" {
		reg, _ := regexp.Compile(".*")
		regex = *reg
	}

	return basicVaultCall[interface{}, []string]("LIST", URL, body, "keys", func(keys interface{}, data map[string]interface{}) ([]string, error) {
		var list = make([]string, 0)
		for _, v := range keys.([]interface{}) {
			if regex.MatchString(v.(string)) {
				list = append(list, v.(string))
			}
		}
		return list, nil
	}, vaultParameter)
}

func VaultCreateKey(vaultParameter VaultKeyTypeParameter) error {

	err := engineExists(vaultParameter.KeyParameter.Vault)

	if err != nil {
		return err
	}

	var resp *http.Response
	method := "POST"

	body := make(map[string]interface{})
	body["type"] = string(vaultParameter.KeyType)
	body["derived"] = false
	body["exportable"] = false

	jsonBody, _ := json.Marshal(body)

	createURL := vaultParameter.KeyParameter.Vault.Client.Address() + "/v1/" +
		vaultParameter.KeyParameter.Vault.EnginePath + "/keys/" + vaultParameter.KeyParameter.KeyName

	request, err := http.NewRequest(method, createURL, strings.NewReader(string(jsonBody)))

	if err != nil {
		return err
	}

	request.Header.Set("Content-type", "application/json")
	request.Header.Set("X-Vault-Token", vaultParameter.KeyParameter.Vault.Client.Token())

	resp, err = http.DefaultClient.Do(request)
	if err == nil {
		defer resp.Body.Close()
		if resp.StatusCode >= 200 && resp.StatusCode <= 300 {
			configURL := vaultParameter.KeyParameter.Vault.Client.Address() + "/v1/" +
				vaultParameter.KeyParameter.Vault.EnginePath + "/keys/" + vaultParameter.KeyParameter.KeyName + "/config"
			configBody := make(map[string]interface{})
			configBody["deletion_allowed"] = true
			configJsonBody, _ := json.Marshal(configBody)

			request, err := http.NewRequest(method, configURL, strings.NewReader(string(configJsonBody)))

			if err != nil {
				return err
			}

			request.Header.Set("Content-type", "application/json")
			request.Header.Set("X-Vault-Token", vaultParameter.KeyParameter.Vault.Client.Token())

			resp, err = http.DefaultClient.Do(request)
			if err == nil {
				defer resp.Body.Close()
				if resp.StatusCode >= 200 && resp.StatusCode <= 300 {
					return nil
				} else {
					err = fmt.Errorf("invalid Status code (%v): (%v)", resp.StatusCode, utils.ExtractHttpBody(resp.Body))
					return err
				}
			} else {
				return err
			}
		} else {
			err = fmt.Errorf("invalid Status code (%v): (%v)", resp.StatusCode, utils.ExtractHttpBody(resp.Body))
			return err
		}
	} else {
		return err
	}
}

func VaultDeleteKey(vaultParameter VaultKeyParameter) error {

	err := engineExists(vaultParameter.Vault)

	if err != nil {
		return err
	}

	method := "DELETE"

	URL := "/v1/" + vaultParameter.Vault.EnginePath + "/keys/" + vaultParameter.KeyName

	request, err := http.NewRequest(method, URL, strings.NewReader(string("")))
	request.Header.Set("X-Vault-Token", vaultParameter.Vault.Client.Token())

	return utils.ProcessRequest(request, err)
}

func VaultRotateKey(vaultParameter VaultKeyParameter) error {
	err := engineExists(vaultParameter.Vault)

	if err != nil {
		return err
	}

	method := "POST"

	URL := "/v1/" + vaultParameter.Vault.EnginePath + "/keys/" + vaultParameter.KeyName + "/rotate"

	request, err := http.NewRequest(method, URL, strings.NewReader(string("")))
	request.Header.Set("X-Vault-Token", vaultParameter.Vault.Client.Token())

	return utils.ProcessRequest(request, err)
}

func VaultHashData(vaultParameter VaultParameter, hashAlgorithm types.HashAlgorithm, data []byte) ([]byte, error) {

	err := engineExists(vaultParameter)

	if err != nil {
		return nil, err
	}

	URL := "/v1/" +
		vaultParameter.EnginePath + "/hash/" + string(hashAlgorithm)

	body := make(map[string]interface{})
	body["input"] = b64.StdEncoding.EncodeToString(data)

	return basicVaultCall("POST", URL, body, "sum", convertB64, vaultParameter)
}

func VaultGenerateRandom(vaultParameter VaultParameter, number int) ([]byte, error) {

	err := engineExists(vaultParameter)

	if err != nil {
		return nil, err
	}

	URL := "/v1/" + vaultParameter.EnginePath + "/random/all/" + strconv.Itoa(number)

	body := make(map[string]interface{})
	body["format"] = "base64"

	return basicVaultCall("POST", URL, body, "random_bytes", convertB64, vaultParameter)
}

func VaultVerifyData(vaultParameter VaultHashParameter, data []byte, signature []byte) (bool, error) {
	if types.ValidateHashFunction(vaultParameter.HashAlgorithm) || vaultParameter.HashAlgorithm == "default" {

		err := engineExists(vaultParameter.KeyParameter.Vault)

		if err != nil {
			return false, err
		}

		URL := "/v1/" +
			vaultParameter.KeyParameter.Vault.EnginePath + "/verify/" +
			vaultParameter.KeyParameter.KeyName

		if vaultParameter.HashAlgorithm != "default" {
			URL = URL + "/" + string(vaultParameter.HashAlgorithm)
		}

		body := make(map[string]interface{})
		body["input"] = b64.StdEncoding.EncodeToString(data)
		body["signature"] = "vault:v1:" + b64.StdEncoding.EncodeToString(signature)

		result, err := basicVaultCall("POST", URL, body, "valid", func(b bool, data map[string]interface{}) (bool, error) {
			return b, nil
		}, vaultParameter.KeyParameter.Vault)

		if err == nil {
			return result, nil
		}

		return false, err
	}
	return false, errors.New("invalid hash function")
}

func VaultSignData(vaultParameter VaultHashParameter, data []byte) ([]byte, error) {

	if types.ValidateHashFunction(vaultParameter.HashAlgorithm) || vaultParameter.HashAlgorithm == "default" {

		err := engineExists(vaultParameter.KeyParameter.Vault)

		if err != nil {
			return nil, err
		}

		URL := "/v1/" +
			vaultParameter.KeyParameter.Vault.EnginePath + "/sign/" +
			vaultParameter.KeyParameter.KeyName

		if vaultParameter.HashAlgorithm != "default" {
			URL = URL + "/" + string(vaultParameter.HashAlgorithm)
		}

		body := make(map[string]interface{})
		body["input"] = b64.StdEncoding.EncodeToString(data)

		return basicVaultCall("POST", URL, body, "signature", convertB64, vaultParameter.KeyParameter.Vault)
	}
	return nil, errors.New("invalid hash function")
}

func VaultEncrypt(vaultParameter VaultKeyParameter, data []byte) ([]byte, error) {

	err := engineExists(vaultParameter.Vault)

	if err != nil {
		return nil, err
	}

	URL := "/v1/" +
		vaultParameter.Vault.EnginePath + "/encrypt/" +
		vaultParameter.KeyName

	body := make(map[string]interface{})
	body["plaintext"] = b64.StdEncoding.EncodeToString(data)

	return basicVaultCall("POST", URL, body, "ciphertext", convertB64, vaultParameter.Vault)
}

func VaultDecrypt(vaultParameter VaultKeyParameter, data []byte) ([]byte, error) {
	err := engineExists(vaultParameter.Vault)

	if err != nil {
		return nil, err
	}

	URL := "/v1/" +
		vaultParameter.Vault.EnginePath + "/decrypt/" +
		vaultParameter.KeyName

	body := make(map[string]interface{})
	body["ciphertext"] = "vault:v1:" + b64.StdEncoding.EncodeToString(data)

	return basicVaultCall[string]("POST", URL, body, "plaintext", convertB64, vaultParameter.Vault)
}

func basicVaultCall[ReturnType any, OutputType any](
	method string,
	url string,
	requestBody map[string]any,
	outputField string,
	transformFunc func(ReturnType, map[string]interface{}) (OutputType, error),
	client VaultParameter) (OutputType, error) {
	jsonBody, _ := json.Marshal(requestBody)

	request, err := http.NewRequest(method, client.Client.Address()+url, strings.NewReader(string(jsonBody)))

	var returnValue OutputType

	if err != nil {
		return returnValue, err
	}

	request.Header.Set("Content-type", "application/json")
	request.Header.Set("X-Vault-Token", client.Client.Token())
	if client.Context != nil {
		request = request.WithContext(client.Context)
	}

	resp, err := http.DefaultClient.Do(request)
	if err == nil {
		defer resp.Body.Close()
		if resp.StatusCode >= 200 && resp.StatusCode <= 300 {
			body := utils.ExtractHttpBody(resp.Body)
			result := make(map[string]interface{})
			err := json.Unmarshal([]byte(body), &result)
			if err == nil {
				data := result["data"]
				out, ok := data.(map[string]interface{})[outputField]
				if ok {
					if transformFunc != nil {
						return transformFunc(out.(ReturnType), data.(map[string]interface{}))
					} else {
						return returnValue, nil
					}
				}
			}
		}
	}
	return returnValue, err
}

func convertB64(val string, data map[string]interface{}) ([]byte, error) {
	var bytes []byte
	var err error
	if strings.Contains(val, "vault:v1:") {
		bytes, err = b64.StdEncoding.DecodeString(val[9:]) //cut of vault:v1:
	} else {
		bytes, err = b64.StdEncoding.DecodeString(val)
	}
	if err == nil {
		return bytes, nil
	}
	return nil, err
}
