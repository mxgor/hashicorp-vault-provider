package vault

import (
	"testing"

	"gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/engine/core/types"
)

func TestGetEmptyNamespaces(t *testing.T) {
	client := VaultGetClient()
	v := VaultParameter{
		Client:     client,
		EnginePath: "fffffff",
	}
	spaces, err := VaultGetNamespaces(v)

	if err != nil {
		t.Fail()
	}

	if len(spaces) > 0 {
		t.Fail()
	}
}

func testKeyCreation(v VaultParameter, t *testing.T, expectCryptoException bool, keyType types.KeyType) *VaultKeyTypeParameter {

	key := VaultKeyTypeParameter{
		KeyParameter: VaultKeyParameter{
			KeyName: "Test" + string(keyType),
			Vault:   v,
		},
		KeyType: keyType,
	}

	err := VaultCreateKey(key)

	if err != nil {

		if expectCryptoException {
			_, ok := err.(*types.CryptoContextError)
			if ok {
				return nil
			}
		}
		t.Fail()
		return nil
	}

	spaces, err := VaultGetNamespaces(v)

	if err != nil {
		t.Fail()
	}

	if len(spaces) == 0 {
		t.Fail()
		return nil
	}

	if spaces[0] != "test" {
		t.Fail()
	}
	return &key
}

func TestGetNameSpaceWithKeyCreationAndCryptoContextCreation(t *testing.T) {

	client := VaultGetClient()
	v := VaultParameter{
		Client:     client,
		EnginePath: "test",
	}

	err := VaultCreateCryptoContext(v)
	if err != nil {
		t.Fail()
		return
	}

	testKeyCreation(v, t, false, types.Rsa2048)

	err = VaultDestroyCryptoContext(v)

	if err != nil {
		t.Fail()
	}
}

func TestGetNamespacesWithKeyCreation(t *testing.T) {
	client := VaultGetClient()
	v := VaultParameter{
		Client:     client,
		EnginePath: "test",
	}
	testKeyCreation(v, t, true, types.Aes256GCM)
}

func TestGetKey(t *testing.T) {
	client := VaultGetClient()
	v := VaultParameter{
		Client:     client,
		EnginePath: "test",
	}

	err := VaultCreateCryptoContext(v)

	if err != nil {
		t.Fail()
	}

	key := testKeyCreation(v, t, false, types.Ed25519)

	desc, err := VaultGetKey(key.KeyParameter)

	if err != nil {
		t.Fail()
	}

	if len(desc) == 0 {
		t.Fail()
	}

	key = testKeyCreation(v, t, false, types.Aes256GCM)

	desc, err = VaultGetKey(key.KeyParameter)

	if err != nil {
		t.Fail()
	}

	if len(desc) == 0 {
		t.Fail()
	}
}
