package vault

import (
	"testing"
)

func Test_CreateClient(t *testing.T) {
	client := VaultGetClient()

	if client == nil {
		t.Fail()
	}
}

func Test_CreateDeleteEngine(t *testing.T) {
	client := VaultGetClient()

	v := VaultParameter{
		Client:      client,
		EnginePath:  "Test",
		Description: "Test2",
	}

	err := VaultCreateEngine(v)

	if err != nil {
		t.Fail()
	}

	b, err := VaultEngineExists(v)

	if err != nil {
		t.Fail()
	}

	if !b {
		t.Fail()
	}

	err = VaultDeleteEngine(v)

	if err != nil {
		t.Fail()
	}

	b, err = VaultEngineExists(v)

	if err != nil {
		t.Fail()
	}

	if b {
		t.Fail()
	}
}
