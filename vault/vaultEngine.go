package vault

import (
	"fmt"
	"net/http"

	"github.com/hashicorp/vault/api"
	vault "github.com/hashicorp/vault/api"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var vaultClient *api.Client

func vaultConfig() VaultClientConfig {
	return VaultClientConfig{
		Address: viper.GetString("VAULT_ADRESS"),
		Token:   viper.GetString("VAULT_TOKEN"),
	}
}

func VaultCreateCryptoContext(v VaultParameter) error {
	if vaultClient != nil {

		b, err := VaultEngineExists(v)

		if err != nil {
			logrus.Fatal(err)
			return nil
		}

		if !b {
			err := VaultCreateEngine(v)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func VaultDestroyCryptoContext(v VaultParameter) error {
	if vaultClient != nil {
		b, err := VaultEngineExists(v)

		if err != nil {
			logrus.Fatal(err)
			return nil
		}

		if b {
			err := VaultDeleteEngine(v)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func VaultGetClient() *vault.Client {

	if vaultClient == nil || vaultClient.Address() == "" {
		viper.SetConfigFile("../.env")
		viper.ReadInConfig()
		viper.AutomaticEnv()
		cfg := vault.DefaultConfig()
		cfg.Address = vaultConfig().Address
		cfg.HttpClient = http.DefaultClient
		client, err := vault.NewClient(cfg)
		token := vaultConfig().Token
		if err != nil {
			logrus.Fatal(err)
		}

		client.SetToken(token)

		if _, err = client.Sys().Capabilities(token, pathSysMounts); err != nil {
			logrus.Fatal(err)
			return nil
		}
		vaultClient = client
	}

	return vaultClient
}

func VaultEngineExists(vaultParameter VaultParameter) (bool, error) {
	client := vaultParameter.Client

	mo, err := client.Sys().ListMounts()

	if err != nil {
		err = fmt.Errorf("unable to find engine: %v", err)
		return false, err
	}

	for k, _ := range mo {
		if k == vaultParameter.EnginePath+"/" {
			return true, nil
		}
	}

	return false, nil
}

func VaultCreateEngine(vaultParameter VaultParameter) error {
	client := vaultParameter.Client

	// Enable engine
	mi := vault.MountInput{}
	mi.Type = "transit"
	if vaultParameter.Description == "" {
		mi.Description = "Auto Generated Engine"
	} else {
		mi.Description = vaultParameter.Description
	}

	err := client.Sys().Mount(vaultParameter.EnginePath, &mi)
	if err != nil {
		err = fmt.Errorf("unable to enable engine: %v", err)
		return err
	}

	return nil
}

func VaultDeleteEngine(vaultParameter VaultParameter) error {
	client := vaultParameter.Client
	// Disable engine
	err := client.Sys().Unmount(vaultParameter.EnginePath)
	if err != nil {
		err = fmt.Errorf("unable to disable engine: %v", err)
		return err
	}

	return nil
}
